# Iris Docker

A simple flask app to predict Iris subspecies based on Sepal and Petal length and width.

https://www.kaggle.com/uciml/iris

The app will listen on port 5000 and accept POST requests to /predict;

```js

{
  "SepalLengthCm": 2,
  "SepalWidthCm": 3,
  "PetalLengthCm": 1,
  "PetalWidthCm": 1
}

```

## Get started

Clone the repo using `git clone https://gitlab.com/andyashall/iris-docker.git`

Login to the [Azure portal](portal.azure.com) and open the cloud shell and run the following commands to create a new resource group and a container registry for your app.

```sh
az group create --name iris-app --location uksouth

az acr create --resource-group iris-app --name iris-app --sku Basic
```
<!-- 
Login to [Azure Devops](dev.azure.com) and create a new project 

Build the container using `sudo docker-compose up`

Tag the container `sudo docker tag {containerid} iris-app.azurecr.io/iris-app`

Push to registry using `sudo docker push iris-app.azurecr.io/iris-app`



 -->

In a perfect world we could simply use docker cli and azure cli to streamline the process but we can get around this by creating devops pipelines and running any azure cli code within a release pipeline

## Overview
1. login to Azure and create a container registry
2. create a container instance using image from registry
3. Create a repo in devops and push code to it 
4. Create a service connection to your azure subscription
5. Create a pipeline to automatically build the container and push the image to your registry on new commit
6. create a release pipeline to re-pull the latest image from the registry after the image has been built and pushed


## Azure Dev ops

### Create release

Trigger on 

Add a stage and a Azure CLI script and enter the below;

`az container restart --resource-group <resource name> --name <container name>`


## Todo

- [ ] Handle multiple objects for prediction
- [ ] Create a train endpoint to save new datapoints and retrain model
- [ ] Tidy up front end
- [ ] [Persist model in external file](https://scikit-learn.org/stable/modules/model_persistence.html)
